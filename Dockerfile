# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

ARG PROJECT_NAME
ARG PROJECT_VERSION
ARG PROJECT_URL

ARG WORKBENCH_IMAGE="registry.gitlab.com/paul_john_king/docker.ubuntu_workbench:0.0.18_7efec42"

ARG GLIBC_REPO="git://sourceware.org/git/glibc.git"
ARG GLIBC_VERSION="2.29"

ARG WORK_DIR="/work"
ARG TARGETS_DIR="${WORK_DIR}/targets"

FROM "${WORKBENCH_IMAGE}" AS WORKBENCH_IMAGE

	ARG GLIBC_REPO
	ARG GLIBC_VERSION

	ARG WORK_DIR
	ARG TARGETS_DIR

	ARG SOURCES_DIR="${WORK_DIR}/sources"
	ARG OBJECTS_DIR="${WORK_DIR}/objects"

	RUN \
		set -e; \
		set -u; \
		mkdir -p \
			"${SOURCES_DIR}" \
			"${OBJECTS_DIR}" \
			"${TARGETS_DIR}"; \
		git clone \
			--depth "1" \
			--branch "release/${GLIBC_VERSION}/master" \
			"${GLIBC_REPO}" "${SOURCES_DIR}"; \
		cd "${OBJECTS_DIR}"; \
		"${SOURCES_DIR}/configure" \
			--prefix="/"; \
		make; \
		make install \
			DESTDIR="${TARGETS_DIR}"; \
		ldconfig -r "${TARGETS_DIR}"; \
		return;

FROM "scratch"

	ARG PROJECT_NAME
	ARG PROJECT_VERSION
	ARG PROJECT_URL

	ARG WORKBENCH_IMAGE

	ARG GLIBC_REPO
	ARG GLIBC_IMAGE

	ARG TARGETS_DIR

	LABEL \
		project.name="${PROJECT_NAME}" \
		project.version="${PROJECT_VERSION}" \
		project.url="${PROJECT_URL}" \
		workbench_image="${WORKBENCH_IMAGE}" \
		glibc.repo="${GLIBC_REPO}" \
		glibc.version="${GLIBC_VERSION}"

	COPY --from="WORKBENCH_IMAGE" "${TARGETS_DIR}" "/"
