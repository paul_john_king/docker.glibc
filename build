#!/bin/sh

# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

set -e ;
set -u ;

_main() {
	local _PROJECT_NAME="docker.glibc";
	local _GIT_REGISTRY="https://gitlab.com/paul_john_king";
	local _DOCKER_REGISTRY="registry.gitlab.com/paul_john_king";

	local _project_description;
	local _project_version;
	local _project_url;
	local _docker_alias;
	local _stage;

	_project_description=$(git describe --long --dirty);
	_project_version="${_project_description%%-*}";
	_project_description="${_project_description#*-}";
	_project_version="${_project_version}.${_project_description%%-*}";
	_project_description="${_project_description#*-g}";
	_project_version="${_project_version}_${_project_description}";
	if test "${_project_version}" != "${_project_version%-dirty}";
	then
		_project_version="${_project_version%-dirty}_dirty";
	fi;

	_project_url="${_GIT_REGISTRY}/${_PROJECT_NAME}";

	_docker_alias="${_DOCKER_REGISTRY}/${_PROJECT_NAME}:${_project_version}";

	for _stage in "Building fresh image";
	do
		echo "${_stage}";
		docker build \
			--build-arg PROJECT_NAME="${_PROJECT_NAME}" \
			--build-arg PROJECT_VERSION="${_project_version}" \
			--build-arg PROJECT_URL="${_project_url}" \
			--tag "${_docker_alias}" \
			"${@}" ".";
	done ;

	return 0 ;
}

_main "${@}" ;
